import React from 'react';
import Plotly from "plotly.js"
import createPlotlyComponent from 'react-plotly.js/factory';
import SearchBar from "material-ui-search-bar";
const Plot = createPlotlyComponent(Plotly);
//have to do next two installs seperately
//npm install  @material-ui/core
//npm install  @material-ui/icons
class Stock extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      stockChartXValues: [],
      stockChartYValues: []
    }
  }

  componentDidMount() {
    document.body.style.backgroundColor = "gray"
    this.fetchStock();
  }

  fetchStock(ticker) {
    const pointerToThis = this;
    console.log(pointerToThis);
    const API_KEY = 'MTOVZZ2ESIOBX50X';
    let StockSymbol = ticker;
    let API_Call = `https://www.alphavantage.co/query?function=TIME_SERIES_DAILY_ADJUSTED&symbol=${StockSymbol}&outputsize=compact&apikey=${API_KEY}`;
    let stockChartXValuesFunction = [];
    let stockChartYValuesFunction = [];

    
    fetch(API_Call)
      .then(
        function(response) {
          return response.json();
        }
      )
      .then(
        function(data) {
          console.log(data);

          for (var key in data['Time Series (Daily)']) {
            stockChartXValuesFunction.push(key);
            stockChartYValuesFunction.push(data['Time Series (Daily)'][key]['1. open']);
          }

          // console.log(stockChartXValuesFunction);
          pointerToThis.setState({
            stockChartXValues: stockChartXValuesFunction,
            stockChartYValues: stockChartYValuesFunction
          });
        }
      )
  }

  render() {
    return (
        
      <div>
   
        <div>
            <h1>Market Open Price Dashboard</h1>
        </div>

        <div>
        <Plot
          data={[
            {
              x: this.state.stockChartXValues,
              y: this.state.stockChartYValues,
              type: 'scatter',
              mode: 'lines+markers',
              marker: {color: 'red'},
            }
          ]}
          layout={{width: 1280, height: 720, title: 'Daily Market Open Prices'}}
        />
        </div>

        <div>
        <strong>Enter stock name below</strong>
        <SearchBar
            value={this.state.value}
            onChange={(newValue) => this.setState({ value: newValue })}
            onRequestSearch={() => this.fetchStock(this.state.value)}
        />
        </div>
        
      </div>
    )
  }
}

export default Stock;