const portfolioServiceUrl = "http://mongodb-rest-application-mongodb-rest-application.dev4.benivade.com/api/v1/portfolio";
function helloWorld(){
    console.log("Hello world!");
}

updatePortfolio();

function updatePortfolio(){
    fetchPortfolio().then(createTable);
    //create Table

}

async function fetchPortfolio(){
    //http GET sent to java program
    const response = await fetch(portfolioServiceUrl);

    //when the response comes back from the java program return the json
    return response.json();
}

document.querySelector('#submit').addEventListener('click', postNewPortfolio);
document.querySelector('#sell').addEventListener('click', postSellNewPortfolio);
document.querySelector('#delete').addEventListener('click', deletePortfolio);
document.querySelector('#get').addEventListener('click', getStockById);


function postNewPortfolio() {
    let currentdate = new Date(); 
    let datetime = currentdate.getDate() + "/"
                + (currentdate.getMonth()+1)  + "/" 
                + currentdate.getFullYear() + " @ "  
                + currentdate.getHours() + ":"  
                + currentdate.getMinutes() + ":" 
                + currentdate.getSeconds(); 
    console.log('button click');
    let portfolioJson = { stockTicker : document.querySelector('#portfolio_stock_ticker').value,
                        price: document.querySelector('#portfolio_stock_price').value, volume: document.querySelector('#portfolio_stock_volume').value,
                         date:datetime, buy_sell:"Buy" }

    doPost(portfolioJson).then(updatePortfolio);
}
function postSellNewPortfolio() {
    let currentdate = new Date(); 
    let datetime = currentdate.getDate() + "/"
                + (currentdate.getMonth()+1)  + "/" 
                + currentdate.getFullYear() + " @ "  
                + currentdate.getHours() + ":"  
                + currentdate.getMinutes() + ":" 
                + currentdate.getSeconds(); 
    console.log('button click');
    let portfolioJson = { stockTicker : document.querySelector('#portfolio_stock_ticker').value,
                        price: document.querySelector('#portfolio_stock_price').value, volume: document.querySelector('#portfolio_stock_volume').value,
                         date:datetime, buy_sell:"Sell" }

    doPost(portfolioJson).then(updatePortfolio);
}

function deletePortfolio() {
    console.log('button click');
    let portfolioId = document.querySelector('#portfolio_stock_id').value;
    doDelete(portfolioId).then(updatePortfolio);
}

function getStockById(){
    console.log('button click');
    let portfolioId = document.querySelector('#portfolio_stock_id').value;
    doGet(portfolioId).then((value) => {
        console.log(value);
        // expected output: "Success!"
      });
}

async function doGet(stockId){
    console.log("Doing Get")
    console.log(stockId);
    return await fetch(portfolioServiceUrl, {
        method: "GET",
        headers :{
            'Content-Type': 'application/json;charset=utf-8'
        },
});
}

async function doDelete(portfolioId) {
    return await fetch(portfolioServiceUrl + '/' + portfolioId, {
                method: "DELETE"
    });
}

async function doPost(portfolioJson) {
    console.log('doing post');
    console.log(portfolioJson);
    return await fetch(portfolioServiceUrl, {
                method: "POST",
                headers :{
                    'Content-Type': 'application/json;charset=utf-8'
                },
                body: JSON.stringify(portfolioJson)
    });
    
}

function createTable(portfolioJson) {
    // create table rows here!!
    console.log(portfolioJson);
    let tableHtml = '<tr><th>stockTicker</th><th>price</th><th>volume</th><th>date @ time</th><th>Buy/Sell</th></tr>'
    portfolioJson.forEach( portfolio => {
        tableHtml += '<tr><td>' + portfolio.stockTicker + '</td><td>' + portfolio.price + '</td><td>' + portfolio.volume + '</td><td>' + portfolio.date + '</td><td>' + portfolio.buy_sell + '</td></tr>' ;
    });
    document.querySelector('#portfolio-table').innerHTML = tableHtml;
}