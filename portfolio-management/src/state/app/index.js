import { createSelector } from 'reselect'
import { DASHBOARD } from '../../utils/menu'

const TOGGLE_DRAWER = 'APP/TOGGLE_DRAWER'
const SELECT_MENU_ITEM = 'APP/SELECT_MENU_ITEM'

// Action creators
export const toggleDrawer = payload => ({ payload, type: TOGGLE_DRAWER })
export const selectMenuItem = payload => ({ payload, type: SELECT_MENU_ITEM })

// Initial state
export const initialState = {
    activePage: DASHBOARD,
    drawerOpened: true
}

const selectAppData = state => state.app

const selectStateFor = key => createSelector(
    selectAppData,
    (app = {}) => app[key]
)

export const selectActivePage = selectStateFor('activePage')
export const selectDrawerOpened = selectStateFor('drawerOpened')

// Reducer
export default (state = initialState, action) => {
    switch (action.type) {
        case TOGGLE_DRAWER:
            console.log('hellow woord')
            return {
                ...state,
                drawerOpened: !state.drawerOpened
            }
        case SELECT_MENU_ITEM:
            return {
                ...state,
                activePage: action.payload
            }
        default:
            return state
    }
}