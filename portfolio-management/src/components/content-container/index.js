import React, { useContext } from 'react'
import { withStyles } from '@material-ui/styles'
import { Container } from '@material-ui/core'
import clsx from 'clsx'
import menuContext from '../../utils/menu-context'

const ContentContainer = ({
    classes,
    children
}) => {
    const { drawerOpened } = useContext(menuContext)

    return (
        <Container className={clsx(classes.content, {
            [classes.contentShift]: drawerOpened,
        })}>
            {children}
        </Container>
    )
}

ContentContainer.prototypes = {
    children: PropTypes.object,
    classes: PropTypes.object,
}

export default withStyles(styles)(ContentContainer)