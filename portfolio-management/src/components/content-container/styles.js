const styles = theme => {
    const drawerWidth = '10%'
    const appBarHeight = '5%'
    return {
        root: {
            display: 'flex',
        },
        content: {
            flexGrow: 1,
            height: `calc(100% - ${appBarHeight})`,
            marginLeft: '0%',
            maxWidth: '100%',
            paddingTop: theme.spacing(2),
            transition: theme.transitions.create('margin', {
                duration: theme.transitions.duration.leavingScreen,
                easing: theme.transitions.easing.sharp,
            }),
            width: '90%'
        },
        contentShift: {
            flexGrow: 1,
            height: `calc(100% - ${appBarHeight})`,
            marginLeft: drawerWidth,
            maxWidth: '100%',
            paddingTop: theme.spacing(2),
            transition: theme.transitions.create('margin', {
                duration: theme.transitions.duration.enteringScreen,
                easing: theme.transitions.easing.easeOut,
            }),
            width: '90%'
        }
    }
}