// App.js
import React, { useMemo, useState, useEffect } from "react";
import { BrowserRouter, useHistory, Switch, Route, Redirect } from 'react-router-dom'
import { PORTFOLIO_DASHBOARD, TRADING_FLOOR, STOCK_CHARTS } from '../../constants/routes'
import MenuContext from '../../utils/menu-context'
import axios from 'axios';

import PortfolioDashboard from "../table";
import PersistentDrawerLeft from "../drawer/Drawer"
import StockChart from "../stockChart/StockChart"
import PortfolioManager from "../portfolio-manager"

const App = props => {
    const history = useHistory()

    return (
        <MenuContext.Provider value={props}>
            <PersistentDrawerLeft />
            <Content />
            {/* <PortfolioDashboard /> */}
        </MenuContext.Provider>
    )
}

App.displayName = 'Portfolio Management.App'

const Content = React.memo(() => (
    <BrowserRouter>
        <Switch>
            <Route exact path={PORTFOLIO_DASHBOARD}><PortfolioDashboard /></Route>
            <Route exact path={STOCK_CHARTS}><StockChart /></Route>
            <Route exact path={TRADING_FLOOR}><PortfolioManager /></Route>
            <Route exact path="/"><Redirect to={PORTFOLIO_DASHBOARD} /></Route>
        </Switch> 
    </BrowserRouter>
))

Content.displayName = 'App.Content'
export default App