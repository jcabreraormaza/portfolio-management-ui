import { connect } from 'react-redux'
import { createStructuredSelector } from 'reselect'
import { selectActivePage, selectDrawerOpened, selectMenuItem, toggleDrawer } from '../../state/app'

const mapStateToProps = createStructuredSelector({
    activePage: selectActivePage,
    drawerOpened: selectDrawerOpened
})

const mapDispatchToProps = ({
    selectMenuItem,
    toggleDrawer
})

export default connect(
    mapStateToProps,
    mapDispatchToProps
)