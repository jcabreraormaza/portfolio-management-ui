import React, { useRef } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import TextField from '@material-ui/core/TextField';
import Paper from '@material-ui/core/Paper';
import Button from '@material-ui/core/Button';
import axios from 'axios';

let {
    STOCK_TICKER,
    PRICE,
    VOLUME
} = ''

const useStyles = makeStyles((theme) => ({
  root: {
    display: 'flex',
    flexWrap: 'wrap',
    height: '100%'
  },
  textField: {
    marginTop: theme.spacing(2),
    marginLeft: theme.spacing(2),
    marginRight: theme.spacing(1),
    width: '25ch',
  },
}));

    export default function LayoutTextFields() {
    const classes = useStyles();
    const ref = useRef()

    const handleSubmit = () => {
       (async () => {
            const result = await axios.post('http://mongodb-rest-application-mongodb-rest-application.dev4.benivade.com/api/v1/portfolio', {
                stockTicker: STOCK_TICKER,
                price: PRICE,
                volume: VOLUME,
                date: '23/7/2021 @12:15:9',
                buy_sell: 'sell'
            })
            .then(function (response) {
                console.log(response);
            }
          )
        })();
        
    }

    const handleStockTickerChange = e => {
        // const { setFieldValue, values } = ref.current.getContext()
        const val = e.target.value
        console.log("val: " + val)
        STOCK_TICKER = val
    }

    const handlePriceChange = e => {
        // const { setFieldValue, values } = ref.current.getContext()
        const val = /^$|^[0-9-+/(/)]+$/.test(e.target.value)
            ? e.target.value : ''
        PRICE = val
    }

    const handleVolumeChange = e => {
        // const { setFieldValue, values } = ref.current.getContext()
        const val = /^$|^[0-9-+/(/)]+$/.test(e.target.value)
            ? e.target.value : ''
        VOLUME = val
    }

    return (
        <div className={classes.root}>
        <div style={{ marginTop: 75}}>
            <Paper className={classes.root}>
                <form className={classes.container} onSubmit={handleSubmit} >
                    <TextField {...{
                        label:"Stock Ticker",
                        id:"margin-none",
                        placeholder: "eg. MSFT",
                        InputProps:{
                            onChange: handleStockTickerChange
                        }
                        }}
                        className={classes.textField}
                    />
                    <TextField {...{
                        label:"Price",
                        id:"margin-dense",
                        placeholder: "eg. 250",
                        InputProps:{
                            onChange: handlePriceChange
                        }
                        }}
                        className={classes.textField}
                    />
                    <TextField {...{
                        label:"Volume",
                        id:"margin-dense",
                        placeholder: "eg. 250",
                        InputProps:{
                            onChange: handleVolumeChange
                        }
                        }}
                        className={classes.textField}
                    />
                </form>

            </Paper>
            <div style={{ margin: 10}}>
                <Button variant="contained" onClick={handleSubmit} color="primary" style={{ marginRight: 10}}>
                    Buy
                </Button>
                
                <Button variant="contained" color="primary">
                    Sell
                </Button>
            </div>
        </div>
        </div>
    );
}
