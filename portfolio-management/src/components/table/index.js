import React from 'react';
import { useMemo, useState, useEffect } from "react";
import axios from 'axios';
import { withStyles, makeStyles } from '@material-ui/core/styles';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';
import styles from './styles'

const StyledTableCell = withStyles((theme) => ({
    head: {
      backgroundColor: theme.palette.common.black,
      color: theme.palette.common.white,
    },
    body: {
      fontSize: 14,
    },
  }))(TableCell);
  
const StyledTableRow = withStyles((theme) => ({
root: {
    '&:nth-of-type(odd)': {
    backgroundColor: theme.palette.action.hover,
    },
},
}))(TableRow);

function createData(stockTicker, price, volume) {
    return { stockTicker, price, volume};
}

const Component = ({
    classes,    
}) => {
    
    const [data, setData] = useState([])
    useEffect(() => {
        (async () => {
            const result = await axios("http://mongodb-rest-application-mongodb-rest-application.dev4.benivade.com/api/v1/portfolio")
            setData(result.data)
        })();
    }, []);

    return (
        <div className={classes.content} >
            <TableContainer component={Paper}>
                <Table className={classes.table} aria-label="customized table">
                    <TableHead>
                        <TableRow>
                            <StyledTableCell>Stock Ticker</StyledTableCell>
                            <StyledTableCell >Price</StyledTableCell>
                            <StyledTableCell >Volume</StyledTableCell>
                            <StyledTableCell >Date</StyledTableCell>
                            <StyledTableCell >Buy/Sell</StyledTableCell>
                        </TableRow>
                    </TableHead>
                    <TableBody>
                        {data.map((row) => (
                            <StyledTableRow key={row.stockTicker}>
                                <StyledTableCell component="th" scope="row">
                                    {row.stockTicker}
                                </StyledTableCell>
                                <StyledTableCell>{row.price}</StyledTableCell>
                                <StyledTableCell>{row.volume}</StyledTableCell>
                                <StyledTableCell>{row.date}</StyledTableCell>
                                <StyledTableCell>{row.buy_sell}</StyledTableCell>
                            </StyledTableRow>
                        ))}
                    </TableBody>
                </Table>
            </TableContainer>
        </div>
    )
}

Component.displayName = 'PortfolioDashboard'

export default withStyles(styles)(Component)