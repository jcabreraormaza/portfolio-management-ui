import React from 'react'

export default React.createContext({
    activePage: '',
    drawerOpened: '',
    selectMenuItem: () => { },
    toggleDrawer: () => { }
})